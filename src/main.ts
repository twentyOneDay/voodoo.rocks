import { createApp } from "vue";
import App from "./App.vue";

import "bootstrap/scss/bootstrap.scss";
import "@/assets/style.scss";

createApp(App).mount("#app");
