import axios from "axios";

const httpApi = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com/",
});

export default httpApi;
